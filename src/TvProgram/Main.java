package TvProgram;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList <TvProgram> commodityArrayList = new ArrayList<>();

        commodityArrayList.add(new Education("Программа о природе", "9:00", "География/Биология"));

        commodityArrayList.add(new Children("Смешарики", "12:00", 0, 18));

        commodityArrayList.add(new Show("Вечер с малаховым", "20:00", "Меллстрой"));

        commodityArrayList.add(new Movie("Титаник", "22:00", "Активный", 1997));

        for(TvProgram com: commodityArrayList){
            System.out.println(com.toString());
        }
    }
}
