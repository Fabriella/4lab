package TvProgram;

public class TvProgram {
    public String name;
    public String time;

    public TvProgram(String name, String time) {
        this.name = name;
        this.time = time;
    }

    @Override
    public String toString() {
        return "TvProgram{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
