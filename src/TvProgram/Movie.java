package TvProgram;

public class Movie extends TvProgram {

    private String directBox;
    private int year;

    public Movie (String name, String time, String directBox, int year) {
        super (name, time);
        this.directBox = directBox;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                "directBox='" + directBox + '\'' +
                ", year=" + year +
                '}';
    }
}
