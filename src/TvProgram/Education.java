package TvProgram;

public class Education extends TvProgram {

    private String science;

    public Education (String name, String time, String science) {
        super(name, time);
        this.science = science;
    }

    @Override
    public String toString() {
        return "Education{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                "science='" + science + '\'' +
                '}';
    }
}
