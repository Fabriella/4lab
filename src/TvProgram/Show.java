package TvProgram;

public class Show extends TvProgram {

    private String theme;

    public Show (String name, String time, String theme) {
        super (name, time);
        this.theme = theme;
    }

    @Override
    public String toString() {
        return "Show{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                "theme='" + theme + '\'' +
                '}';
    }
}
