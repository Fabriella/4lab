package TvProgram;

public class Children extends TvProgram {

    private int minAge;
    private int maxAge;

    public Children (String name, String time, int minAge, int maxAge) {
        super (name, time);
        this.minAge = minAge;
        this.maxAge = maxAge;
    }

    @Override
    public String toString() {
        return "Children{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                "minAge=" + minAge +
                ", maxAge=" + maxAge +
                '}';
    }
}
